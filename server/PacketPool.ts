import UnconnectedPing from "../protocol/UnconnectedPing";
import OpenConnectionRequest1 from "../protocol/OpenConnectionRequest1";
import OpenConnectionRequest2 from "../protocol/OpenConnectionRequest2";

export default class PacketPool extends Map {
    constructor(){
        super();
        this.registerPackets();
    }

    registerPacket(packet){
        this.set(packet.getId(), packet);
    }

    getPacket(id){
        return this.has(id) ? this.get(id) : null;
    }

    registerPackets(){
        this.registerPacket(UnconnectedPing);
        this.registerPacket(OpenConnectionRequest1);
        this.registerPacket(OpenConnectionRequest2);
    }
}