import Session from "./Session";

import BinaryStream from "binarystream";

import OfflineMessage from "../protocol/OfflineMessage";
import OfflineMessageHandler from "./OfflineMessageHandler";

import BITFLAG from "../protocol/BitFlags";
import ACK from "../protocol/ACK";
import NACK from "../protocol/NACK";
import Datagram from "../protocol/Datagram";

import PacketPool from "./PacketPool";

export default class SessionManager {

    private _packetPool: PacketPool;
    private _server;
    private _socket;
    private _bytes;
    private _sessions;
    private _offlineMessageHandler;
    private _shutdown;
    private _ticks;
    private _lastMeasure;
    private _blocked;
    private portChecking;
    private startTime;
    private _startTime;
    private _outgoingMessages;

    static get RAKNET_TPS(){return 100}
    static get RAKNET_TICK_LENGTH(){return 1 / SessionManager.RAKNET_TPS}

    public initVars(){
        this._packetPool = new PacketPool();

        this._server = null;
        this._socket = null;

        this._bytes = {
            received: 0,
            sent: 0
        };

        this._sessions = new Map();
        
        this._offlineMessageHandler = null;
        
        this._shutdown = false;

        this._ticks = 0;
        this._lastMeasure = -1;

        this._blocked = new Map();

        this.portChecking = false;

        this.startTime = -1;

        this._outgoingMessages = [];
    }

    constructor(server, socket) {
        this.initVars();

        this._server = server;
        this._socket = socket;

        this._startTime = Date.now();

        this._offlineMessageHandler = new OfflineMessageHandler(this);

        this.start();
    }

    public start(){
        this._socket.getSocket().on("message", (msg, rinfo) => {
            this._bytes.received += msg.length;

            if(this._blocked.has(rinfo.address)){
                return;
            }

            if(msg.length < 1){
                return;
            }

            let stream = new BinaryStream(msg);

            let packetId = stream.getBuffer()[0];

            //this.logger.debug("Received", packetId, "with length of", msg.length, "from", rinfo.address + ":" + rinfo.port);

            this.handle(packetId, stream, rinfo.address, rinfo.port);
        });

        this.tickProcessor();
    }

    public getTimeSinceStart(){
        return Date.now() - this._startTime;
    }

    public getPort(){
        return this._server.getPort();
    }

    public getLogger(){
        return this._server.getLogger();
    }

    public shutdown(){
        this._shutdown = true;
    }

    public tickProcessor(){
        this._lastMeasure = Date.now();

        let int = setInterval(() => {
            if(!this._shutdown){
                this.tick();
            }else{
                clearInterval(int);
            }
        }, SessionManager.RAKNET_TICK_LENGTH * 1000);
    }

    public tick(){
        let time = Date.now();

        for(let [,session] of this._sessions){
            session.update(time);
        }

        if((this._ticks % SessionManager.RAKNET_TPS) === 0){
            let diff = Math.max(0.005, time - this._lastMeasure);
            let bandwidth = {
                up: this._bytes.sent / diff,
                down: this._bytes.received / diff
            };

            this._lastMeasure = time;
            this._bytes.sent = 0;
            this._bytes.received = 0;

            if(this._blocked.size > 0){
                let now = Date.now();
                for(let [address, timeout] of this._blocked){
                    if(timeout <= now){
                        this._blocked.delete(address);
                    }else{
                        break;
                    }
                }
            }
        }

        ++this._ticks;
    }

    public getId(){
        return this._server.getId();
    }

    public getServerName(){
        return this._server.getServerName();
    }

    public sendPacket(packet, address, port){

        packet.encode();
        if(address instanceof Session) this._bytes.sent += this._socket.sendBuffer(packet.getStream().getBuffer(), address.getAddress(), address.getPort());
        else this._bytes.sent += this._socket.sendBuffer(packet.getStream().getBuffer(), address, port);

        //this.getLogger().debug("Sent "+protocol.constructor.name+"("+protocol.stream.buffer.toString("hex")+") to "+address+":"+port);
    }

    public createSession(address, port, clientId, mtuSize){
        let session = new Session(this, address, port, clientId, mtuSize);
        this._sessions.set(SessionManager.hashAddress(address, port), session);
        this.getLogger().debug(`Created session for ${session.toString()} with MTU size ${mtuSize}`);
        return session;
    }

    public sessionExists(address, port){
        if(address instanceof Session) return this._sessions.has(SessionManager.hashAddress(address.getAddress(), address.getPort()));
        else return this._sessions.has(SessionManager.hashAddress(address, port));
    }

    public removeSession(session, reason = "unknown"){
        let id = SessionManager.hashAddress(session.getAddress(), session.getPort());
        if(this._sessions.has(id)){
            this._sessions.get(id).close();
            this.removeSessionInternal(this);
            this.sendOutgoingMessage({
                purpose: "closeSession",
                data: {
                    identifier: id,
                    reason: reason
                }
            });
        }
    }

    public removeSessionInternal(session){
        this._sessions.delete(session.toString());
    }

    public getSession(address, port){
        if(this.sessionExists(address, port)) return this._sessions.get(SessionManager.hashAddress(address, port));
        else return null;
    }

    public getSessionByIdentifier(identifier){
        return this._sessions.get(identifier);
    }

    public getSessions(){
        return Array.from(this._sessions.values());
    }

    public openSession(session){
        this.sendOutgoingMessage({
            purpose: "openSession",
            data: {
                identifier: session.toString(),
                ip: session.getAddress(),
                port: session.getPort(),
                clientId: session.clientId
            }
        });
    }

    public handle(packetId, stream, ip, port){
        let session = this.getSession(ip, port);

        //console.log("got packet!", stream);

        if(session === null){
            let packet = this._packetPool.getPacket(packetId);
            if(packet !== null && (packet = new packet(stream))){
                if(packet instanceof OfflineMessage){
                    packet.decode();
                    if(packet.validMagic()){
                        if(!this._offlineMessageHandler.handle(packet, ip, port)){
                            this.getLogger().debug("Received unhandled offline message " + packet.constructor.name + " from " + session);
                        }
                    }else{
                        this.getLogger().debug("Received invalid message from " + session + ":", "0x" + packet.getBuffer().toString("hex"));
                    }
                }
            }
        }else{
            if((packetId & BITFLAG.VALID) === 0){
                this.getLogger().debug("Ignored non-connected message for " + session + " due to session already opened");
            }else{
                if(packetId & BITFLAG.ACK){
                    session.handlePacket(new ACK(stream));
                }else if(packetId & BITFLAG.NAK){
                    session.handlePacket(new NACK(stream));
                }else{
                    session.handlePacket(new Datagram(stream));
                }
            }
        }
    }

    public blockAddress(address, timeout = 300){
        let final = Date.now() + timeout;
        if(!this._blocked.has(address) || timeout !== -1){
            if(timeout === -1){
                let final = Number.MAX_SAFE_INTEGER;
            }else{
                this.getLogger().notice(`Blocked ${address} for ${timeout} seconds`);
            }
            this._blocked.set(address, final);
        }else if(this._blocked.get(address) < final){
            this._blocked.set(address, final);
        }
    }

    public unblockAddress(address){
        this._blocked.delete(address);
        this.getLogger().debug(`Unblocked ${address}`);
    }

    public sendOutgoingMessage(message){
        this._outgoingMessages.push(message);
    }

    public readOutgoingMessages(){
        let tmp = this._outgoingMessages;
        this._outgoingMessages = [];
        return tmp;
    }

    public static hashAddress(ip, port){
        return `${ip}:${port}`;
    }
}