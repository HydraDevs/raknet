import Packet from "../protocol/Packet";

import RakNet from "../RakNet";
import BinaryStream from "binarystream";

import Datagram from "../protocol/Datagram";
import EncapsulatedPacket from "../protocol/EncapsulatedPacket";
import ConnectionRequest from "../protocol/ConnectionRequest";
import ConnectionRequestAccepted from "../protocol/ConnectionRequestAccepted";
import NewIncomingConnection from "../protocol/NewIncomingConnection";
import ConnectedPing from "../protocol/ConnectedPing";
import ConnectedPong from "../protocol/ConnectedPong";
import DisconnectionNotification from "../protocol/DisconnectionNotification";

import PacketReliability from "../protocol/PacketReliability";

import ACK from "../protocol/ACK";
import NACK from "../protocol/NACK";

import RecoveryQueue from "./queues/RecoveryQueue";
import ACKQueue from "./queues/ACKQueue";
import NACKQueue from "./queues/NACKQueue";
import SplitQueue from "./queues/SplitQueue";
import PacketBatchHolder from "./queues/PacketBatchHolder";

import MessageIdentifiers from "../protocol/MessageIdentifiers";

export default class Session {

    private sessionManager;
    private address;
    private port;
    private state;
    private mtuSize;
    private clientId;
    private lastSequenceNumber;
    private currentSequenceNumber;
    private messageIndex;
    private channelIndex;
    private splitId;
    private lastUpdate;
    private disconnectionTime;
    private isActive;
    private packetsToSend;
    private _sendQueue;
    private recoveryQueue;
    private ACKQueue;
    private NACKQueue;
    private splitQueue;
    private packetBatches;
    private lastPingTime;
    private lastPingMeasure;

    static get STATE_CONNECTING(){return 0}
    static get STATE_CONNECTED(){return 1}
    static get STATE_DISCONNECTING(){return 2}
    static get STATE_DISCONNECTED(){return 3}

    get MAX_SPLIT_SIZE(){
        return 128;
    }

    get MAX_SPLIT_COUNT(){
        return 4;
    }

    initVars(){
        this.sessionManager = {};

        this.address = "";
        this.port = -1;
        this.state = Session.STATE_CONNECTING;
        this.mtuSize = -1;
        this.clientId = -1;

        this.lastSequenceNumber = -1;
        this.currentSequenceNumber = 0;

        this.messageIndex = 0;
        this.channelIndex = [];

        this.splitId = 0;

        this.lastUpdate = 0;
        this.disconnectionTime = -1;

        this.isActive = false;

        this.packetsToSend = [];

        this._sendQueue = {};
        this.recoveryQueue = new RecoveryQueue();
        this.ACKQueue = new ACKQueue();
        this.NACKQueue = new NACKQueue();
        this.splitQueue = new SplitQueue();
        this.packetBatches = new PacketBatchHolder();

        this.lastPingMeasure = 1;
    }

    constructor(sessionManager, address, port, clientId, mtuSize){
        this.initVars();
        this.sessionManager = sessionManager;

        this.address = address;
        this.port = port;
        this.clientId = clientId;
        this.mtuSize = mtuSize;

        this.setSendQueue();

        this.lastUpdate = Date.now();
    }

    setSendQueue(){
        this._sendQueue = new Datagram();
        this._sendQueue.needsBAndAs = true;
    }

    getAddress(){
        return this.address;
    }

    getPort(){
        return this.port;
    }

    getClientId(){
        return this.clientId;
    }

    isConnecting(){
        return this.state === Session.STATE_CONNECTING;
    }

    isConnected(){
        return this.state !== Session.STATE_DISCONNECTING && this.state !== Session.STATE_DISCONNECTED;
    }

    setConnected(){
        this.state = Session.STATE_CONNECTED;
        this.lastUpdate = Date.now();
        this.sessionManager.getLogger().debug(this+" is now connected.");
    }

    update(time){
        if(!this.isActive && (this.lastUpdate + 10000) < time){
            this.disconnect("timeout");
            return;
        }

        if(this.state === Session.STATE_DISCONNECTING && (
            (this.ACKQueue.isEmpty() && this.NACKQueue.isEmpty() && this.packetsToSend.length === 0 && this.recoveryQueue.isEmpty()) &&
            this.disconnectionTime + 10 < time)
        ){
            this.close();
            return;
        }

        this.isActive = false;

        if(!this.ACKQueue.isEmpty()){
            let pk = new ACK();
            pk.packets = this.ACKQueue.getAll();
            this.sendPacket(pk);
            this.ACKQueue.clear();
        }

        if(!this.NACKQueue.isEmpty()){
            let pk = new NACK();
            pk.packets = this.NACKQueue.getAll();
            this.sendPacket(pk);
            this.NACKQueue.clear();
        }

        if(this.packetsToSend.length > 0){
            let limit = 16;
            for(let k in this.packetsToSend){
                this.sendDatagram(this.packetsToSend[k]);
                delete this.packetsToSend[k];

                if(--limit <= 0){
                    break;
                }
            }
        }

        if(this.lastPingTime + 5000 < time){
            this.sendPing();
            this.lastPingTime = time;
        }

        this.sendQueue();
    }


    close(){
        if(this.state !== Session.STATE_DISCONNECTED){
            this.state = Session.STATE_DISCONNECTED;

            this.queueConnectedPacket(new DisconnectionNotification(), PacketReliability.RELIABLE_ORDERED, 0, RakNet.PRIORITY_IMMEDIATE);

            this.sessionManager.getLogger().debug(`Closed session for ${this.toString()}`);
            this.sessionManager.removeSessionInternal(this);
            this.sessionManager = null;
        }
    }

    disconnect(reason = "unknown"){
        this.sessionManager.removeSession(this, reason);
    }

    handlePacket(packet){
        this.isActive = true;
        this.lastUpdate = Date.now();

        if(packet instanceof Datagram || packet instanceof ACK || packet instanceof NACK){
            //this.sessionManager.getLogger().debug("Got " + protocol.constructor.name + "(" + protocol.stream.buffer.toString("hex") + ") from " + this);
        }

        if(packet instanceof Datagram){
            packet.decode();

            let diff = packet.sequenceNumber - this.lastSequenceNumber;

            if(!this.NACKQueue.isEmpty()){
                this.NACKQueue.remove(packet.sequenceNumber);
                if(diff !== 1){
                    for(let i = this.lastSequenceNumber + 1; i < packet.sequenceNumber; i++){
                        this.NACKQueue.add(i);
                    }
                }
            }

            this.ACKQueue.add(packet.sequenceNumber);

            if(diff >= 1){
                this.lastSequenceNumber = packet.sequenceNumber;
            }

            packet.packets.forEach(pk => this.handleEncapsulatedPacket(pk));
        }else{
            if(packet instanceof ACK){
                packet.decode();
                this.recoveryQueue.recover(packet.packets).forEach(datagram => {
                    this.recoveryQueue.remove(datagram.sequenceNumber);
                });
            }else if(packet instanceof NACK){
                packet.decode();
                this.recoveryQueue.recover(packet.packets).forEach(datagram => {
                    this.packetsToSend.push(datagram);
                    this.recoveryQueue.remove(datagram.sequenceNumber);
                });
            }
        }
    }

    handleEncapsulatedPacket(packet){
        if(!(packet instanceof EncapsulatedPacket)) throw new TypeError("Expecting EncapsulatedPacket, got "+(packet.constructor.name ? packet.constructor.name : packet));

        //this.sessionManager.getLogger().debug("Handling EncapsulatedPacket("+protocol.getBuffer().toString("hex")+")["+protocol.getBuffer().length+"] from "+this);

        if(packet.hasSplit){
            if(this.isConnected()) this.handleSplitPacket(packet);
            return;
        }

        let id = packet.getBuffer()[0];
        let dpk, pk;
        switch(id){
            case ConnectionRequest.getId():
                this.sessionManager.getLogger().debug("Got ConnectionRequest from "+this);
                dpk = new ConnectionRequest(packet.getStream());
                dpk.decode();

                this.clientId = dpk.clientId;

                pk = new ConnectionRequestAccepted();
                pk.address = this.getAddress();
                pk.port = this.getPort();
                pk.sendPingTime = dpk.sendPingTime;
                pk.sendPongTime = this.sessionManager.getTimeSinceStart();
                this.queueConnectedPacket(pk, PacketReliability.UNRELIABLE, 0, RakNet.PRIORITY_IMMEDIATE);
                break;

            case NewIncomingConnection.getId():
                this.sessionManager.getLogger().debug("Got NewIncomingConnection from "+this);

                dpk = new NewIncomingConnection(packet.getStream());
                dpk.decode();

                if(true || dpk.port === this.sessionManager.getPort()){//todo: if port checking
                    this.setConnected();

                    this.sessionManager.openSession(this);

                    this.sendPing();
                }
                break;

            case ConnectedPing.getId():
                dpk = new ConnectedPing(packet.getStream());
                dpk.decode();

                pk = new ConnectedPong();
                pk.sendPingTime = dpk.sendPingTime;
                pk.sendPongTime = this.sessionManager.getTimeSinceStart();
                this.queueConnectedPacket(pk, PacketReliability.UNRELIABLE, 0);
                break;

            case ConnectedPong.getId():
                dpk = new ConnectedPong(packet.getStream());
                dpk.decode();

                this.handlePong(dpk.sendPingTime, dpk.sendPongTime);
                break;

            case DisconnectionNotification.getId():
                this.disconnect("client disconnect"); //supposed to send ack
                break;

            case MessageIdentifiers.MINECRAFT_HEADER:
                this.packetBatches.add(packet);
                this.sessionManager.getLogger().debug("Got a Minecraft packet");
                break;

            default:
                this.packetBatches.add(packet);
                this.sessionManager.getLogger().debug("Got unknown packet: ", id);
                break;
        }
    }

    handlePong(ping, pong){
        this.lastPingMeasure = this.sessionManager.getTimeSinceStart() - ping;
    }

    handleSplitPacket(packet){
        if(!(packet instanceof EncapsulatedPacket)) throw new TypeError("Expecting EncapsulatedPacket, got "+(packet.constructor.name ? packet.constructor.name : packet));

        if(packet.splitCount >= this.MAX_SPLIT_SIZE || packet.splitIndex >= this.MAX_SPLIT_SIZE || packet.splitIndex < 0){
            return;
        }

        if(this.splitQueue.size >= this.MAX_SPLIT_COUNT) return;
        this.splitQueue.add(packet);

        if(this.splitQueue.getSplitSize(packet.splitId) === packet.splitCount){
            let pk = new EncapsulatedPacket();
            let stream = new BinaryStream();
            let packets = this.splitQueue.getSplits(packet.splitId);
            for(let [splitIndex, packet] of packets){
                stream.append(packet.getBuffer());
            }
            this.splitQueue.remove(packet.splitId);

            pk.stream = stream.flip();
            pk.length = stream.offset;

            this.handleEncapsulatedPacket(pk);
        }
    }

    sendPacket(pk){
        if(pk instanceof Packet){
            this.sessionManager.sendPacket(pk, this);
            return true;
        }
        return false;
    }

    sendDatagram(datagram){
        if(!(datagram instanceof Datagram)) throw new TypeError("Expecting Datagram, got "+(datagram.constructor.name ? datagram.constructor.name : datagram));

        if(datagram.sequenceNumber !== null){
            this.recoveryQueue.remove(datagram.sequenceNumber);
        }
        datagram.sequenceNumber = this.currentSequenceNumber++;
        datagram.sendTime = Date.now();
        this.recoveryQueue.addRecoveryFor(datagram);
        this.sendPacket(datagram);
    }

    sendPing(reliability = PacketReliability.UNRELIABLE){
        let pk = new ConnectedPing();
        pk.sendPingTime = this.sessionManager.getTimeSinceStart();
        this.queueConnectedPacket(pk, reliability, 0, RakNet.PRIORITY_IMMEDIATE);
    }

    queueConnectedPacket(packet, reliability, orderChannel = 1, flags = RakNet.PRIORITY_NORMAL){
        packet.encode();

        let pk = new EncapsulatedPacket();
        pk.reliability = reliability;
        pk.orderChannel = orderChannel;
        pk.stream = new BinaryStream(packet.getBuffer());

        //this.sessionManager.getLogger().debug("Queuing "+protocol.constructor.name+"("+protocol.getBuffer().toString("hex")+")");

        this.addEncapsulatedToQueue(pk, flags);
    }

    queueConnectedPacketFromServer(packet, needACK, immediate){
        return this.queueConnectedPacket(packet, (needACK === true ? RakNet.FLAG_NEED_ACK : 0) | (immediate === true ? RakNet.PRIORITY_IMMEDIATE : RakNet.PRIORITY_NORMAL));
    }

    addEncapsulatedToQueue(packet, flags){
        if(!(packet instanceof EncapsulatedPacket)) throw new TypeError("Expecting EncapsulatedPacket, got "+(packet.constructor.name ? packet.constructor.name : packet));

        if(packet.isReliable()){
            packet.messageIndex = this.messageIndex++;
        }

        if(packet.isSequenced()){
            packet.orderIndex = this.channelIndex[packet.orderChannel]++;
        }

        let maxSize = this.mtuSize - 60;

        if(packet.getBuffer().length > maxSize){
            let splitId = ++this.splitId % 65536;
            let splitIndex = 0;
            let splitCount = Math.ceil(packet.getBuffer().length / maxSize);
            while(!packet.getStream().feof()){
                let stream = packet.getBuffer().slice(packet.getStream().offset, packet.getStream().offset += maxSize);
                let pk = new EncapsulatedPacket();
                pk.splitId = splitId;
                pk.hasSplit = true;
                pk.splitCount = splitCount;
                pk.reliability = packet.reliability;
                pk.splitIndex = splitIndex;
                pk.stream = stream;

                if (splitIndex > 0) {
                    pk.messageIndex = this.messageIndex++;
                } else {
                    pk.messageIndex = packet.messageIndex;
                }

                pk.orderChannel = packet.orderChannel;
                pk.orderIndex = packet.orderIndex;

                this.addToQueue(pk, flags | RakNet.PRIORITY_IMMEDIATE);
                splitIndex++;
            }
        }else{
            this.addToQueue(packet, flags);
        }
    }

    addToQueue(pk, flags = RakNet.PRIORITY_NORMAL){
        let priority = flags & 0x07;

        let length = this._sendQueue.getLength();
        if((length + pk.getLength()) > (this.mtuSize - 36)){
            this.sendQueue();
        }

        if(pk.needACK){
            this._sendQueue.packets.push(Object.assign(new EncapsulatedPacket(), pk));
            pk.needACK = false;
        }else{
            this._sendQueue.packets.push(pk.toBinary());
        }

        if(priority === RakNet.PRIORITY_IMMEDIATE){
            this.sendQueue();
        }
    }

    sendQueue(){
        if(this._sendQueue.packets.length > 0){
            this.sendDatagram(this._sendQueue);
            this.setSendQueue();
        }
    }

    toString(){
        return this.address + ":" + this.port;
    }
}