import UDPServerSocket from "./UDPServerSocket";
import SessionManager from "./SessionManager";
import PacketPool from "./PacketPool";
import ServerName from "./ServerName";

export default class RakNetServer {

    private _port: number;
    private _logger: any;
    private _shutdown: boolean;
    private _server: UDPServerSocket;
    private _sessionManager: SessionManager;
    private _serverName: ServerName;
    private _packetPool: PacketPool;

    initVars(){
        this._port = -1;
        this._logger = null;

        this._shutdown = false;

        this._server = null;
        this._sessionManager = null;

        this._serverName = new ServerName();
        this._packetPool = new PacketPool();
    }

    constructor(port, logger){
        this.initVars();

        if(port < 1 || port > 65536){
            throw new Error("Invalid port range");
        }

        this._port = port;
        this._logger = logger;

        this._server = new UDPServerSocket(port, logger);
        this._sessionManager = new SessionManager(this, this._server);
    }

    isShutdown(){
        return this._shutdown === true;
    }

    shutdown(){
        this._shutdown = true;
    }

    getPort(){
        return this._port;
    }

    getServerName(){
        return this._serverName;
    }

    getLogger(){
        return this._logger;
    }

    getId(){
        return this.getServerName().getServerId();
    }

    getSessionManager(){
        return this._sessionManager;
    }

    getPacketPool(){
        return this._packetPool;
    }
}