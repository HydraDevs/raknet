import EncapsulatedPacket from "../../protocol/EncapsulatedPacket";

export default class SplitQueue extends Map {
    add(pk){
        pk instanceof EncapsulatedPacket;

        if(this.has(pk.splitId)){
            let m = this.get(pk.splitId);
            m.set(pk.splitIndex, pk);
            this.set(pk.splitId, m);
        }else{
            let m = new Map([[pk.splitIndex, pk]]);
            this.set(pk.splitId, m);
        }
    }

    getSplitSize(splitId){
        splitId instanceof Number;

        return this.get(splitId).size;
    }

    getSplits(splitId){
        splitId instanceof Number;

        return this.get(splitId);
    }

    remove(splitId){
        this.delete(splitId);
    }

    isEmpty(){
        return this.size === 0;
    }
}