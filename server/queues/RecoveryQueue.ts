import Datagram from "../../protocol/Datagram";

export default class RecoveryQueue extends Map {
    addRecoveryFor(datagram){
        datagram instanceof Datagram;

        this.set(datagram.sequenceNumber, datagram);
    }

    isRecoverable(seqNumber){
        seqNumber instanceof Number;

        return this.has(seqNumber);
    }

    recover(sequenceNumbers){
        sequenceNumbers instanceof Array;

        let datagrams = [];

        sequenceNumbers.forEach(seqNumber => {
            if(this.isRecoverable(seqNumber)){
                datagrams.push(this.get(seqNumber));
            }
        });

        return datagrams;
    }

    remove(seqNumber){
        this.delete(seqNumber);
    }

    isEmpty(){
        return this.size === 0;
    }
}