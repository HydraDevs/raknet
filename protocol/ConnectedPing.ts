import Packet from "./Packet";
import MessageIdentifiers from "./MessageIdentifiers";

export default class ConnectedPing extends Packet {

    private sendPingTime;

    static getId(){
        return MessageIdentifiers.ID_CONNECTED_PING;
    }

    constructor(stream){
        super(stream);
        this.sendPingTime = -1;
    }

    encodePayload(){
        this.getStream()
            .writeLong(this.sendPingTime);
    }

    decodePayload(){
        this.sendPingTime = this.getStream().readLong();
    }
}