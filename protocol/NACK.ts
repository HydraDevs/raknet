import AcknowledgementPacket from "./AcknowledgementPacket";

export default class NACK extends AcknowledgementPacket {
    static getId(){
        return 0xA0;
    }

    constructor(stream){
        super(stream);
    }
}