import OfflineMessage from "./OfflineMessage";
import MessageIdentifiers from "./MessageIdentifiers";

export default class IncompatibleProtocolVersion extends OfflineMessage {

    public protocolVersion;
    public serverId;

    static getId(){
        return MessageIdentifiers.ID_INCOMPATIBLE_PROTOCOL_VERSION;
    }

    initVars(){
        this.protocolVersion = -1;
        this.serverId = -1;
    }

    constructor(){
        super();
        this.initVars();
    }

    encodePayload(){
        this.getStream().writeByte(this.protocolVersion);

        this.writeMagic();

        this.getStream().writeLong(this.serverId);
    }
}