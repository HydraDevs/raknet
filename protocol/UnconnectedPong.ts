import OfflineMessage from "./OfflineMessage";
import MessageIdentifiers from "./MessageIdentifiers";

export default class UnconnectedPong extends OfflineMessage {

    public serverName;
    public serverId;
    public pingId;

    static getId(){
        return MessageIdentifiers.ID_UNCONNECTED_PONG;
    }

    initVars() {
        this.serverName = "";
        this.serverId = -1;
        this.pingId = -1;
    }


    constructor(){
        super();
        this.initVars();
    }
    
    encodePayload(){
        this.getStream()
            .writeLong(this.pingId)
            .writeLong(this.serverId);

        this.writeMagic();

        this.getStream()
            .writeShort(this.serverName.length)
            .writeString(this.serverName);
    }
}