import Packet from "./Packet";
import MessageIdentifiers from "./MessageIdentifiers";

export default class DisconnectionNotification extends Packet {
    static getId(){
        return MessageIdentifiers.ID_DISCONNECTION_NOTIFICATION;
    }
}