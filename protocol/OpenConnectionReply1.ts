import OfflineMessage from "./OfflineMessage";
import MessageIdentifiers from "./MessageIdentifiers";

export default class OpenConnectionReply1 extends OfflineMessage {

    public serverId;
    public serverSecurity;
    public mtuSize;

    static getId(){
        return MessageIdentifiers.ID_OPEN_CONNECTION_REPLY_1;
    }

    initVars(){
        this.serverId = -1;
        this.serverSecurity = false;
        this.mtuSize = -1;
    }

    constructor(){
        super();
        this.initVars();
    }

    encodePayload(){
        this.writeMagic();
        this.getStream()
            .writeLong(this.serverId)
            .writeBool(this.serverSecurity)
            .writeShort(this.mtuSize);
    }
}