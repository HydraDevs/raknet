import AcknowledgementPacket from "./AcknowledgementPacket";

export default class ACK extends AcknowledgementPacket {
    static getId(){
        return 0xc0;
    }

    constructor(stream){
        super(stream);
        if(stream) this.stream = stream;
    }
}